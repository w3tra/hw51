const Movie = (props) => {
    return (
        <div className="movie">
            <img src={props.logo} className="Logo" alt="logo" />
            <h1>{props.name}</h1>
            <p>Year: {props.year}</p>
        </div>
    );
};

const container = document.getElementById('app');

const movies = (
    <React.Fragment>
        <Movie name="South Park" year="1997" logo="img/south.jpg"/>
        <Movie name="Harry Potter and the Half-Blood Prince" year="2009" logo="img/harry.jpg"/>
        <Movie name="Watchmen" year="2009" logo="img/watchmen.jpg"/>
    </React.Fragment>
);

ReactDOM.render(movies, container);