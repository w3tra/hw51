const container = document.getElementById('app');

const Movie = (props) => {
    return (
        <div className="movie">
            <img src={props.logo} className="Logo" alt="logo" />
            <h1>{props.name}</h1>
            <p>Year: {props.year}</p>
        </div>
    );
};

const Logo = (props) => {
  return (
      <a
          className="logo"
          href="#"
          target="_blank"
          rel="noopener noreferrer"
      >
          Logo Here
      </a>
  );
};

const NavLink = (props) => {
    return (
        <a href = "#">
            {props.text}
        </a>
    )
};

const headerElement = (
    <React.Fragment>
        <header id="header">
            <Logo logo="img/logo.png" alt="PayPaul"/>
            <nav className="main-nav">
                <NavLink text="Buy"/>
                <NavLink text="Sell"/>
                <NavLink text="Manage"/>
            </nav>
        </header>
        <div className="movies">
            <React.Fragment>
                <Movie name="South Park" year="1997" logo="img/south.jpg"/>
                <Movie name="Harry Potter and the Half-Blood Prince" year="2009" logo="img/harry.jpg"/>
                <Movie name="Watchmen" year="2009" logo="img/watchmen.jpg"/>
            </React.Fragment>
        </div>
        <footer>
            <Logo logo="img/logo.png" alt="PayPaul"/>
            <nav className="main-nav">
                <NavLink text="Buy"/>
                <NavLink text="Sell"/>
                <NavLink text="Manage"/>
            </nav>
        </footer>
    </React.Fragment>
);

ReactDOM.render(headerElement, container);
